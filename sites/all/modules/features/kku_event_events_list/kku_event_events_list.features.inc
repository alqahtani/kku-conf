<?php
/**
 * @file
 * kku_event_events_list.features.inc
 */

/**
 * Implements hook_views_api().
 */
function kku_event_events_list_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function kku_event_events_list_node_info() {
  $items = array(
    'event_details' => array(
      'name' => t('Event Details'),
      'base' => 'node_content',
      'description' => t('This content type should include general information about the event, name, link, type, logo and details '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
