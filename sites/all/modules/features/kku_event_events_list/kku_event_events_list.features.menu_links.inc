<?php
/**
 * @file
 * kku_event_events_list.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function kku_event_events_list_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_previous:previous.
  $menu_links['main-menu_previous:previous'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'previous',
    'router_path' => 'previous',
    'link_title' => 'Previous',
    'options' => array(
      'identifier' => 'main-menu_previous:previous',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );
  // Exported menu link: main-menu_upcoming:upcoming.
  $menu_links['main-menu_upcoming:upcoming'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'upcoming',
    'router_path' => 'upcoming',
    'link_title' => 'Upcoming',
    'options' => array(
      'identifier' => 'main-menu_upcoming:upcoming',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Previous');
  t('Upcoming');

  return $menu_links;
}
