<?php

/**
 * @file
 *
 * Moxiemanager enables users to preview images and conduct basic file
 * operations on them, such as upload and delete.
 * The Moxiemanager is an online file management utility
 */

/**
 * Implements hook_permission().
 */
function moxiemanager_permission() {
  return array(
    'administer moxiemanager' => array(
      'title' => t('Administer Moxiemanager'),
      'description' => t('Administer Moxiemanager'),
    ),
    'administer user file browsers' => array(
      'title' => t('Administer user file browsers'),
      'description' => t('Set custom paths users have access to.'),
    ),
    'access moxiemanager files' => array(
      'title' => t('Access files through the Moxiemanager file browser'),
      'description' => t('Access files through the Moxiemanager file browser'),
    ),
  );
}

/**
 * Implements hook_help().
 */
function moxiemanager_help($path, $arg) {
  switch ($path) {
    // Main module help for the Moxiemanager module
    // @todo
    case 'admin/help#moxiemanager':
      return '<p>' . t('Need some description here') . '</p>';
  }
}

/**
 * Implements hook_menu().
 */
function moxiemanager_menu() {
  $items['admin/config/media/moxiemanager'] = array(
    'title' => 'Moxiemanager settings',
    'description' => 'Configure Moxiemanager.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('moxiemanager_settings_form'),
    'access arguments' => array('administer moxiemanager'),
    'file' => 'moxiemanager.pages.inc',
  );
  $items['admin/config/media/moxiemanager/reset'] = array(
    'title' => 'Reset Moxiemanager settings',
    'description' => 'Configure Moxiemanager.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('moxiemanager_settings_reset_form'),
    'access arguments' => array('administer moxiemanager'),
    'file' => 'moxiemanager.pages.inc',
  );
  $items['moxiemanager_auth_drupal'] = array(
    'page callback' => 'moxiemanager_auth_drupal',
    'access arguments' => array('access moxiemanager files'),
    'file' => 'moxiemanager.pages.inc',
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/moxiemanager'] = array(
    'title' => 'Filebrowser',
    'page callback' => 'moxiemanager_user_file_browser',
    'page arguments' => array(1),
    'access callback' => 'moxiemanager_user_file_browser_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Callback for the user-specific file browser page.
 */
function moxiemanager_user_file_browser($account) {
  $library_path = _moxiemanager_library_directory();
  $module_path = drupal_get_path('module', 'moxiemanager');

  $url = url($library_path . '/index.php', array('query' => array('uid' => $account->uid)));

  $build = array();
  $build['moxiemanager'] = array(
    '#markup' => '<iframe id="moxiemanager_user_filebrowser" src="' . $url . '"></iframe>',
    '#weight' => -10,
    '#attached' => array(
      'css' => array(
        $module_path . '/moxiemanager_filebrowser.css',
      ),
    ),
  );
  return $build;
}

/**
 * Access callback for loading the user-specific filebrowser.
 */
function moxiemanager_user_file_browser_access($account) {
  global $user;

  if (user_access('administer user file browsers')) {
    return TRUE;
  }

  if (($user->uid == $account->uid) && user_access('access moxiemanager files')) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_theme().
 */
function moxiemanager_theme() {
  return array(
    'moxiemanager_settings_form' => array(
      'render element' => 'form',
      'file' => 'moxiemanager.pages.inc',
    ),
    'moxiemanager_paths' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Implements hook_field_extra_fields().
 */
function moxiemanager_field_extra_fields() {
  $extra['user']['user']['form']['moxiemanager'] = array(
    'label' => t('Moxiemanager'),
    'description' => t('Moxiemanager.'),
    'weight' => 3,
  );
  return $extra;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allow users to decide if tracking code will be added to pages or not.
 */
function moxiemanager_form_user_profile_form_alter(&$form, &$form_state) {
  $account = $form['#user'];
  $category = $form['#user_category'];

  // Add a wrapper for the choices and more button.
  $form['moxiemanager'] = array(
    '#tree' => FALSE,
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Moxiemanager file browser paths'),
    '#title' => t('Here you can set the paths which are accessible for this user.'),
    '#prefix' => '<div class="clearfix" id="moxiemanager-paths-wrapper">',
    '#suffix' => '</div>',
    '#access' => user_access('administer user file browsers'),
  );

  // Container for just the moxiemanager paths.
  $form['moxiemanager']['moxiemanager_paths'] = array(
    '#prefix' => '<div id="moxiemanager-paths">',
    '#suffix' => '</div>',
    '#theme' => 'moxiemanager_paths',
  );

  if (isset($form_state['moxiemanager_paths_count'])) {
    $moxiemanager_paths_count = $form_state['moxiemanager_paths_count'];
  }
  else {
    $moxiemanager_paths_count = max(1, empty($account->data['moxiemanager']) ? 1 : count($account->data['moxiemanager']));
  }

  // Add the current choices to the form.
  $delta = 0;
  $weight = 0;
  if (isset($account->data['moxiemanager'])) {
    $delta = count($account->data['moxiemanager']);
    foreach ($account->data['moxiemanager'] as $pid => $path) {
      $key = 'pid:' . $pid;
      $form['moxiemanager']['moxiemanager_paths'][$key] = _moxiemanager_path_form($key, $path['pid'], $path['label'], $path['value'], $path['weight'], $moxiemanager_paths_count);
      $weight = max($path['weight'], $weight);
    }
  }
  // Add initial or additional choices.
  $existing_delta = $delta;
  for ($delta; $delta < $moxiemanager_paths_count; $delta++) {
    $key = 'new:' . ($delta - $existing_delta);
    // Increase the weight of each new choice.
    $weight++;
    $form['moxiemanager']['moxiemanager_paths'][$key] = _moxiemanager_path_form($key, NULL, '', '', $weight, $moxiemanager_paths_count);
  }

  $form['moxiemanager']['moxiemanager_more'] = array(
    '#type' => 'submit',
    '#value' => t('More choices'),
    '#attributes' => array(
      'title' => t("If the amount of boxes above isn't enough, click here to add more choices."),
    ),
    '#weight' => 1,
    '#limit_validation_errors' => array(array('moxiemanager_paths')),
    '#submit' => array('moxiemanager_more_paths_submit'),
    '#ajax' => array(
      'callback' => 'moxiemanager_paths_js',
      'wrapper' => 'moxiemanager-paths',
      'effect' => 'fade',
    ),
  );
}

/**
 * Implements hook_user_presave().
 */
function moxiemanager_user_presave(&$edit, $account, $category) {
  if (isset($edit['moxiemanager_paths'])) {
    foreach ($edit['moxiemanager_paths'] as $delta => $path) {
      if (empty($path['label']) || empty($path['value'])) {
        unset($edit['moxiemanager_paths'][$delta]);
      }
    }

    if (empty($edit['moxiemanager_paths'])) {
      unset($edit['data']['moxiemanager']);
    }
    else {
      $edit['data']['moxiemanager'] = $edit['moxiemanager_paths'];
    }
  }
}

/**
 * Returns HTML for an admin poll form for choices.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_moxiemanager_paths($variables) {
  $form = $variables['form'];

  drupal_add_tabledrag('moxiemanager-paths-table', 'order', 'sibling', 'moxiemanager-path-weight');

  $delta = 0;
  $rows = array();
  $headers = array('', t('Label'), t('Path'));
  $headers[] = t('Weight');

  foreach (element_children($form) as $key) {
    $delta++;
    // Set special classes for drag and drop updating.
    $form[$key]['weight']['#attributes']['class'] = array('moxiemanager-path-weight');

    // Build the table row.
    $row = array(
      'data' => array(
        array('class' => array('moxiemanager-path-value')),
        drupal_render($form[$key]['label']),
        drupal_render($form[$key]['value']),
      ),
      'class' => array('draggable'),
    );
    $row['data'][] = drupal_render($form[$key]['weight']);

    // Add any additional classes set on the row.
    if (!empty($form[$key]['#attributes']['class'])) {
      $row['class'] = array_merge($row['class'], $form[$key]['#attributes']['class']);
    }

    $rows[] = $row;
  }

  $output = theme('table', array('header' => $headers, 'rows' => $rows, 'attributes' => array('id' => 'moxiemanager-paths-table')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Form definition callback.
 */
function _moxiemanager_path_form($key, $pid = NULL, $label = '', $value = '', $weight = 0, $size = 10) {
  $form = array(
    '#tree' => TRUE,
    '#weight' => $weight,
  );

  // We'll manually set the #parents property of these fields so that
  // their values appear in the $form_state['values']['choice'] array.
  $form['pid'] = array(
    '#type' => 'value',
    '#value' => $pid,
    '#parents' => array('moxiemanager_paths', $key, 'pid'),
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => $value !== '' ? t('Label') : t('New label'),
    '#title_display' => 'invisible',
    '#default_value' => $label,
    '#parents' => array('moxiemanager_paths', $key, 'label'),
  );

  $form['value'] = array(
    '#type' => 'textfield',
    '#title' => $value !== '' ? t('Path') : t('New path'),
    '#title_display' => 'invisible',
    '#default_value' => $value,
    '#parents' => array('moxiemanager_paths', $key, 'value'),
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => $value !== '' ? t('Weight for path @label', array('@label' => $label)) : t('Weight for new path'),
    '#title_display' => 'invisible',
    '#default_value' => $weight,
    '#delta' => $size,
    '#parents' => array('moxiemanager_paths', $key, 'weight'),
  );

  return $form;
}


/**
 * Submit handler to add more choices to a moxiemanager form.
 *
 * This handler is run regardless of whether JS is enabled or not. It makes
 * changes to the form state. If the button was clicked with JS disabled, then
 * the page is reloaded with the complete rebuilt form. If the button was
 * clicked with JS enabled, then ajax_form_callback() calls moxiemanager_paths_js() to
 * return just the changed part of the form.
 */
function moxiemanager_more_paths_submit($form, &$form_state) {
  // If this is a Ajax POST, add 1, otherwise add 5 more choices to the form.
  if ($form_state['values']['moxiemanager_more']) {
    $n = $_GET['q'] == 'system/ajax' ? 1 : 5;
    $form_state['moxiemanager_paths_count'] = count($form_state['values']['moxiemanager_paths']) + $n;
  }
  // Renumber the choices. This invalidates the corresponding key/value
  // associations in $form_state['input'], so clear that out. This requires
  // moxiemanager_form() to rebuild the choices with the values in
  // $form_state['node']->choice, which it does.
  $form_state['user']->data['moxiemanager'] = array_values($form_state['values']['moxiemanager_paths']);
  unset($form_state['input']['moxiemanager_paths']);
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback in response to new choices being added to the form.
 *
 * This returns the new page content to replace the page content made obsolete
 * by the form submission.
 *
 * @see moxiemanager_more_paths_submit()
 */
function moxiemanager_paths_js($form, $form_state) {
  return $form['moxiemanager']['moxiemanager_paths'];
}

/**
 * Implements hook_wysiwyg_plugin().
 */
function moxiemanager_wysiwyg_plugin($editor, $version) {
  switch ($editor) {
    case 'tinymce':
    case 'ckeditor':
      if (user_access('access moxiemanager files')) {
        $plugins = array();
        $library_path = _moxiemanager_library_directory();

        $plugins['moxiemanager'] = array(
          'url' => 'admin/config/media/moxiemanager',
          'path' => $library_path,
          'filename' => 'editor_plugin.js',
          'buttons' => array(
            'moxiemanager' => t('Moxiemanager'),
          ),
          'options' => array(
            'moxiemanager_remove_script_host' => TRUE,
          ),
          'load' => TRUE,
          'internal' => FALSE,
        );
        return $plugins;
      }
      break;
  }
}

function moxiemanager_wysiwyg_editor_settings_alter(&$settings, $context) {
  $buttons = $context['profile']->settings['buttons'];
  if (!empty($buttons['moxiemanager']) && $buttons['moxiemanager']['moxiemanager']) {
    $module_path = drupal_get_path('module', 'moxiemanager');
    $library_path = _moxiemanager_library_directory();
    drupal_add_js($library_path . '/js/moxman.loader.min.js', array('preprocess' => FALSE));

    // Check if the ckeditor module exists.
    if (module_exists('ckeditor')) {
      drupal_add_js($module_path . '/ckeditor_plugins/moxiemanager/plugin.js', array('preprocess' => FALSE));
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function moxiemanager_node_presave($node) {
  // Check if the current format uses our module's filter
  if (isset($node->format) && filter_uses_moxiemanager_filter($node->format)) {

    // Strip base paths from image sources
    if ($node->body && !empty($node->body)) {
      $node->body = moxiemanager_filter_process($node->body, $node->format, TRUE);
    }
    if ($node->teaser && !empty($node->teaser)) {
      $node->teaser = moxiemanager_filter_process($node->teaser, $node->format, TRUE);
    }

    // If the CCK module is enabled, we need to process these fields as well
    if (module_exists('content')) {
      // Gather type information.
      $type = content_types($node->type);

      // Loop through each content field
      foreach ($type['fields'] as $name => $field) {

        // Only proceed if the element exists for this node
        if (isset($node->{$name})) {

          // Since fields allow multiple values, we need to walk trough each element
          foreach ($node->{$name} as $k => $field_data) {

            // Only strip the base paths if the field's filter equals the node's filter
            if ($node->format == $node->{$name}[$k]['format']) {
              $node->{$name}[$k]['value'] = moxiemanager_filter_process($node->{$name}[$k]['value'], $node->format, TRUE);
            }
          }
        }
      }
    }
  }
}

/**
 * Helper function for checking if a given format uses the 'replace base paths' filter
 *
 * @param $format (int)
 *   - The ID of the node's input format
 */
function filter_uses_moxiemanager_filter($format) {
  $filters = filter_list_format($format);
  foreach ($filters as $filter) {
    if ($filter->module == 'moxiemanager') {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Replaces or adds base paths from image tags
 *
 * @param $text (string)
 *   - The node's body which is used to search for image tags
 *
 * @param $format (int)
 *   - The ID of the node's input format
 *
 * @param $strip (bool)
 *   - Whether or not to strip the base paths from image tags
 *
 * @see moxiemanager_nodeapi(), moxiemanager_filter()
 */
function moxiemanager_filter_process($text, $format, $strip = FALSE) {
  $pattern = '/(\<img)(.+?)(src=")(.+?)\"(.+?)(\/\>|\>)/i';
  if (preg_match_all($pattern, $text, $matches)) {
    $base_path = base_path();
    foreach ($matches[0] as $key => $match) {
      if ($strip) {
        $text = str_replace($matches[3][$key] . $base_path, $matches[3][$key], $text);
      }
      else {
        $text = str_replace($matches[3][$key] . $matches[4][$key], $matches[3][$key] . $base_path . $matches[4][$key], $text);
      }
    }
  }
  return $text;
}

/**
 * Checks the moxiemanager module requirements
 */
function moxiemanager_check_status() {
  module_load_include('install', 'moxiemanager');
  $requirements = module_invoke('moxiemanager', 'requirements', 'runtime');
  return isset($requirements['moxiemanager_ok']);
}

/**
 * Default configuration options for Moxiemanager.
 *
 * All elements here will be editable in the configuration form on admin/settings/moxiemanager
 */
function moxiemanager_config_defaults() {
  // Paste here all your default settings

  require_once(DRUPAL_ROOT . '/includes/file.inc');
  $wrappers = file_get_stream_wrappers();
  $rootpath = array();

  $library_directory = _moxiemanager_library_directory();
  $library_directory_relative = str_replace(DRUPAL_ROOT, '', $library_directory);
  $prefix = str_repeat('../', count(explode('/', $library_directory_relative)));

  // No need to include the tmp dir.
  unset($wrappers['temporary']);

  // @todo: Fix private file system
  //unset($wrappers['private']);

  foreach($wrappers as $wrapper_key => $wrapper) {
    $real_path = drupal_realpath($wrapper_key . '://');
    if (strpos($real_path, DRUPAL_ROOT) === 0) {
      $relative_path = $prefix . trim(str_replace(DRUPAL_ROOT, '', $real_path), '/');
    }
    else {
      $relative_path = $real_path;
    }

    $rootpath[] = $wrapper['name'] . '=' . $relative_path;
  }
  $config['filesystem.rootpath'] = implode(';', $rootpath);
  $config['filesystem.local.wwwroot'] = DRUPAL_ROOT;

  // If base authenticator is set to 'ExternalAuthenticator':
  // $config['ExternalAuthenticator.secret_key'] = 'someSecretKey';

  drupal_alter('moxiemanager_config_defaults', $config);

  return $config;
}

/**
 * Searches for a libraries directory
 */
function _moxiemanager_library_directory() {

  if (module_exists('libraries')) {
    $dir = libraries_get_path('moxiemanager');
    return is_dir($dir) ? $dir : FALSE;
  }
  elseif (module_exists('jqp')) {
    $dir = variable_get('jqp_directory', 'sites/all/libraries');
    return is_dir($dir) ? $dir : FALSE;
  }
  elseif ($dir = variable_get('moxiemanager_directory', FALSE)) {
    return is_dir($dir) ? $dir : FALSE;
  }

  global $profile;
  $config = conf_path();

  $searchdir = array(
    'libraries/moxiemanager',
    'sites/all/libraries/moxiemanager',
    'profiles/' . $profile . '/libraries/moxiemanager',
    $config . '/libraries/moxiemanager',
  );

  foreach ($searchdir as $dir) {
    if (is_dir($dir)) {
      variable_set('moxiemanager_directory', $dir);
      return $dir;
    }
  }
  return FALSE;
}

/**
 * Scans the file- or imagemanager library directories for available language
 * files and stores the available languages in the cache.
 * Used in auth_drupal.php to set the appropriate language.
 */
function moxiemanager_languages() {
  // Firtst try to load the language settings from the cache.
  if ($cache = cache_get('moxiemanager_languages')) {
    return $cache->data;
  }
  else {
    // Not found in the cache.
    // Scan the libraries directories for available translation files.
    $library_path = DRUPAL_ROOT . '/' . _moxiemanager_library_directory();
    $moxiemanager_languages = array();

    $language_path = $library_path . '/language/' . $data->type;
    $files = file_scan_directory($language_path, '/.*\.xml/', array('key' => 'name'));
    $languages[$data->type] = array_keys($files);

    // Set the cache to avoid scanning the language directories on every page
    // load.
    cache_set('moxiemanager_languages', $languages);
    return $languages;
  }

}

/**
 * Implementation of hook_element_info_alter().
 */
function moxiemanager_element_info_alter(&$types) {
  $types['text_format']['#pre_render'][] = 'moxiemanager_pre_render_text_format';
}

/**
 *
 */
function moxiemanager_pre_render_text_format($element) {
  // Check if the ckeditor module exists.
  if (module_exists('ckeditor')) {
    //$global_profile = ckeditor_profile_load('CKEditor Global Profile');
    $profile = ckeditor_get_profile($element['#format']);

    if (!empty($profile->settings['loadPlugins']['moxiemanager'])) {
      $library_path = _moxiemanager_library_directory();
      $element['#attached']['js'][$library_path . '/js/moxman.loader.min.js'] = array(
        'preprocess' => FALSE,
      );
    }
  }

  return $element;
}

/**
 * Implements hook_ckeditor_plugin().
 */
function moxiemanager_ckeditor_plugin() {
  $plugin = array();
  $module_path = drupal_get_path('module', 'moxiemanager');
  $plugin['moxiemanager'] = array(
    'name' => 'moxiemanager',
    'desc' => t('Plugin for the Moxiemanager'),
    'path' => $module_path . '/ckeditor_plugins/moxiemanager/',
  );
  return $plugin;
}

/**
 * Retrieves the configuration array from Moxiemanager's config.php file.
 */
function moxiemanager_plugin_config() {
  $library_path = _moxiemanager_library_directory();
  require($library_path . '/config.php');
  return isset($moxieManagerConfig) ? $moxieManagerConfig : NULL;
}
